﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PeruzalTest
{
    class ConvertToWords
    {
        public static string[] breakDownTheNumber(string number)
        {
            string[] splitted = new string[0];
            if(number.Contains(","))
            {
                splitted = number.Split(',');
            }
            else
            {
                splitted = number.Split('.');
            }

            return splitted;
            
        }

        public static string convertToWords(string number)
        {
            int _number = Convert.ToInt32(number);
            string result = "";
            if (_number < 10)
            {
                result = NumberToString.processOnes(_number);
            }
            else if (_number < 20)
            {
                result = NumberToString.processTens(_number);

            }
            else if(_number < 100)
            {
                result = NumberToString.processHigherTens(_number);
            }
            else if(_number < 1000)
            {
                result = NumberToString.processHundreds(_number);
            }
            else if (_number < 10000)
            {
                result = NumberToString.processOneThousands(_number);
            }
            else
            {
                result = "You are surpassing my conversion limits";
            }

            return result;
        }
    }
}
