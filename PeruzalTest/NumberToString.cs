﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PeruzalTest
{
    class NumberToString
    {
        public static string processTens(int number)
        {
            switch (number)
            {
                case 10:
                    return "Ten";
                case 11:
                    return "Eleven";
                case 12:
                    return "Twelve";
                case 13:
                    return "Thirteen";
                case 14:
                    return "Fourteen";
                case 15:
                    return "Fifteen";
                case 16:
                    return "Sixteen";
                case 17:
                    return "Seventeen";
                case 18:
                    return "Eighteen";
                case 19:
                    return "Nineteen";
                default:
                    return "This number is not in the 10s";
            }
        }

        public static string processHigherTens(int number)
        {
            int intTen = (int) Math.Floor((double)(number / 10));
            string strTen = "";

            switch (intTen)
            {
                case 2:
                    strTen = "Twenty";
                    break;
                case 3:
                    strTen = "Thirty";
                    break;
                case 4:
                    strTen = "Fourty";
                    break;
                case 5:
                    strTen = "Fifty";
                    break;
                case 6:
                    strTen = "Sixty";
                    break;
                case 7:
                    strTen = "Seventy";
                    break;
                case 8:
                    strTen = "Eighty";
                    break;
                case 9:
                    strTen = "Ninty";
                    break;
                default:
                    return "This number is not in the higher 10s";
            }
            if (number % 10 == 0)
            {
                return strTen;
            }
            else
            {
                var remainder = number % 10;
                var strOne = NumberToString.processOnes(remainder);
                return $"{strTen} {strOne}";
            }
        }

        public static string processOnes(int number)
        {
            switch (number)
            {
                case 1:
                    return "One";
                case 2:
                    return "Two";
                case 3:
                    return "Three";
                case 4:
                    return "Four";
                case 5:
                    return "Five";
                case 6:
                    return "Six";
                case 7:
                    return "Seven";
                case 8:
                    return "Eight";
                case 9:
                    return "Nine";
                default:
                    return "This number is not in the ones";
            }
        }

        public static string processHundreds(int number)
        {
            var hundredth = (int)Math.Floor((double)(number / 100));
            var tenth = number % 100;

            var result = NumberToString.processOnes(hundredth) + " hundred";
            if (tenth != 0)
            {
                if (tenth < 20)
                {
                    result += " and " + NumberToString.processTens(tenth);
                }
                else
                {
                    result += " and " + NumberToString.processHigherTens(tenth);
                }
            }

            return result;
        }

        public static string processOneThousands(int number)
        {
            var thousandth = (int)Math.Floor((double)(number / 1000));
            var hundreth = number % 1000;

            var result = NumberToString.processOnes(thousandth) + " thousand ";
            if (hundreth != 0)
            {
                result += NumberToString.processHundreds(hundreth);
            }

            return result;
        }
    }
}
