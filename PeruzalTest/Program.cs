﻿using System;

namespace PeruzalTest
{
    public class CurrencyConvert
    {
        static void Main(string[] args)
        {
            Console.Write("Please enter a number: ");
            string number = Console.ReadLine();

            if (!number.Contains("-") && !number.Equals(""))
            {

                Console.WriteLine(getResults(number));
            }
            else 
            {
                Console.WriteLine("Invalid number");
            }

            Console.ReadLine();
        }

        public static string getResults(string number)
        {
            string result = "";
            string[] data = null;
            if (number.Contains(",") || number.Contains("."))
            {
                data = ConvertToWords.breakDownTheNumber(number);

                string beforeDecimal = ConvertToWords.convertToWords(data[0]);
                string afterDecimal = ConvertToWords.convertToWords(data[1]);

                result = beforeDecimal + " Rand and " + afterDecimal + " cents";
            }
            else
            {
                string beforeDecimal = ConvertToWords.convertToWords(number);
                result = beforeDecimal + " Rand";
            }
            return result;
        }
    }
}