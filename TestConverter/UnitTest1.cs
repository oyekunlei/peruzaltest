using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PeruzalTest;

namespace TestConverter
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var result = CurrencyConvert.getResults("20");
            Assert.AreEqual(result, "Twenty Rand");
        }
    }
}
